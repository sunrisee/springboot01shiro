package com.jizi.mapper;

import com.jizi.pojo.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author 宋朝阳
 * @create 2020 -11 -28 23:13
 */
@Repository
@Mapper
public interface SysUserMapper {
    public SysUser queryUser(String name);
}
