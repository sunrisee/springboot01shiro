package com.jizi.service;

import com.jizi.mapper.SysUserMapper;
import com.jizi.pojo.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 宋朝阳
 * @create 2020 -11 -28 23:19
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    SysUserMapper sysUserMapper;

    @Override
    public SysUser queryUser(String name) {
        return sysUserMapper.queryUser(name);
    }
}
