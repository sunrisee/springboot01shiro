package com.jizi.service;

import com.jizi.pojo.SysUser;

/**
 * @author 宋朝阳
 * @create 2020 -11 -28 23:18
 */
public interface UserService {
    public SysUser queryUser(String name);
}
