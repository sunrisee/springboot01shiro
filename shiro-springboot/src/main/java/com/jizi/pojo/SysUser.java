package com.jizi.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 宋朝阳
 * @create 2020 -11 -28 23:12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysUser {
    private Integer userid;
    private String loginname;
    private String identity;
    private String realname;
    private int sex;
    private String address;
    private String phone;
    private String pwd;
    private String position;
    private Integer type;
    private Integer available;
    private String salt;

}
